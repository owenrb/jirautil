/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package net.friartuck.jira.parser;

import net.friartuck.jira.xml.model.CiItem;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

import java.io.FileReader;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class CiList
{
	//~ Methods ----------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param   xmlFile  DOCUMENT ME!
	 * @return  DOCUMENT ME!
	 */
	public List<CiItem> process(String xmlFile, String outputFormat)
	{
		List<CiItem> data = new ArrayList<>();

		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new FileReader(xmlFile)));

			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression exprItems = xpath.compile(
					"/rss/channel/item");

			XPathExpression exprKey = xpath.compile(
					"key");
			XPathExpression exprSeverity = xpath.compile(
					"priority");
			XPathExpression exprSummary = xpath.compile(
					"summary");
			XPathExpression exprFreshdesk = xpath.compile(
					"customfields/customfield[@id='customfield_12180']/customfieldvalues/customfieldvalue");

			XPathExpression exprComments = xpath.compile(
					"comments");

			XPathExpression exprVersion = xpath.compile(
					"version");

			XPathExpression exprFixVersion = xpath.compile(
					"fixVersion");

			NodeList list = (NodeList) exprItems.evaluate(document, XPathConstants.NODESET);
			for (int i = 0; i < list.getLength(); i++)
			{
				CiItem ci = new CiItem();

				Node node = list.item(i);

				// Jira Ticket #
				String key = exprKey.evaluate(node, XPathConstants.STRING).toString();
				ci.setJira(key);

				// Description & IssueSummary
				String summary = exprSummary.evaluate(node, XPathConstants.STRING).toString();
				ci.setSummary(summary);

				// Severity
				String severity = exprSeverity.evaluate(node, XPathConstants.STRING).toString();
				ci.setSeverity(severity);

				// Freshdesk Ticket #
				String freshdesk = exprFreshdesk.evaluate(node, XPathConstants.STRING).toString();
				ci.setFreshdesk(Util.filterFreshdesk(freshdesk));

				// Affected Version (issue is identified)
				String affectedVersion = exprVersion.evaluate(node, XPathConstants.STRING)
					.toString();
				ci.setAffected(affectedVersion);

				String fixVersion = exprFixVersion.evaluate(node, XPathConstants.STRING).toString();
				ci.setPatchVersion(fixVersion);

				NodeList commentList = (NodeList) exprComments.evaluate(node,
						XPathConstants.NODESET);

				for (int k = 0; k < commentList.getLength(); k++)
				{
					Node nodeComment = commentList.item(k);
					String comment = nodeComment.getTextContent();

					if (comment.contains("]]"))
					{
						comment = comment.replaceAll("\n", "");

						if (!"HTML".equals(outputFormat))
						{
							comment = comment.replaceAll("<br/>", "\n");
							comment = comment.replaceAll("&nbsp;", "").replaceAll("&quot;", "'")
								.replaceAll("&#39;", "\"").replaceAll("&amp;", "and");
						}

						// Client
						String client = Util.getInfo("Client", comment);
						if (client != null)
						{
							ci.setClient(client);
						}

						// Team
						String team = Util.getInfo("Team", comment);
						if (team != null)
						{
							ci.setTeam(team);
						}

						// Assignee
						String assignee = Util.getInfo("Assignee", comment);
						if (assignee != null)
						{
							ci.setAssignee(assignee);
						}

						// Work week
						String workWeek = Util.getInfo("WorkWeek", comment);
						if (workWeek != null)
						{
							ci.setWorkWeek(workWeek);
						}

						// Release target
						String releaseTarget = Util.getInfo("ReleaseTarget", comment);
						if (releaseTarget != null)
						{
							ci.setReleaseTarget(releaseTarget);
						}

						// Jira Status
						String jiraStatus = Util.getInfo("JiraStatus", comment);
						if (jiraStatus != null)
						{
							ci.setStatus(jiraStatus);
						}

						// Resolution
						String resolution = Util.getInfo("Resolution", comment);
						if (resolution != null)
						{
							ci.setResolution(resolution);
						}

						// Fix/Patch Version

						// Comments
						String comments = Util.getInfo("Comments", comment);
						StringBuilder sb = new StringBuilder();
						if (ci.getComments() != null)
						{
							sb.append(ci.getComments());
						}

						if (comments != null)
						{
							sb.append("\n");
							sb.append(comments);
						}
						ci.setComments(sb.toString());

						// Summary2
						String summary2 = Util.getInfo("Summary", comment);
						StringBuilder sb2 = new StringBuilder();
						if (ci.getSummary() != null)
						{
							sb2.append(ci.getSummary());
						}

						if (summary2 != null)
						{
							sb2.append("\n");
							sb2.append(summary2);
						}
						ci.setSummary(sb2.toString());
					} // end if
				} // end for

				data.add(ci);
			} // end for
		}
		catch (Exception e)
		{
			e.printStackTrace();
		} // end try-catch

		return data;
	}
}
