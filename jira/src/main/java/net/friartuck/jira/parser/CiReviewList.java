/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package net.friartuck.jira.parser;

import net.friartuck.jira.xml.model.CiItem;

import java.util.List;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class CiReviewList
{
	//~ Methods ----------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param  args  DOCUMENT ME!
	 */
	public static void main(String[] args)
	{
		String xmlFile = "C:\\Users\\Admin\\Downloads\\SearchRequest.xml";

		CiList ciList = new CiList();
		List<CiItem> data = ciList.process(xmlFile, Util.OUTPUT_FORMAT);

		if ("HTML".equals(Util.OUTPUT_FORMAT))
		{
			String html = printHtml(data);
			System.out.println(html);
		}
		else
		{
			String xml = printXml(data);
			System.out.println(xml);
		}
	}

	//~ ------------------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param   data  DOCUMENT ME!
	 * @return  DOCUMENT ME!
	 */
	public static String printHtml(List<CiItem> data)
	{
		StringBuilder sb = new StringBuilder();

		sb.append("<html><body>");
		sb.append("<table border='1'>");
		sb.append("\n");

		// print header

		sb.append("<tr>");
		sb.append("\n");

		// Client
		sb.append(Util.printElement("", "client"));

		// Freshdesk Ticket #
		sb.append(Util.printElement("", "freshdesk"));

		// Jira Ticket #
		sb.append(Util.printElement("", "jira"));

		// Severity
		sb.append(Util.printElement("", "severity"));

		// Description & IssueSummary
		sb.append(Util.printElement("", "summary"));

		// Team
		sb.append(Util.printElement("", "team"));

		// Assignee
		sb.append(Util.printElement("", "assignee"));

		// Release Target (Wed)
		sb.append(Util.printElement("", "releaseTarget"));

		// Jira Status
		sb.append(Util.printElement("", "jiraStatus"));

		// Resolution
		sb.append(Util.printElement("", "resolution"));

		// Affected Version (issue is identified)
		sb.append(Util.printElement("", "affectedVersion"));

		// Fix/Patch Version
		sb.append(Util.printElement("", "fixVersion"));

		// Comments
		sb.append(Util.printElement("", "comments"));

		sb.append("</tr>");
		sb.append("\n");

		for (CiItem ci : data)
		{
			sb.append("<tr>");
			sb.append("\n");

			// Client
			sb.append(Util.printElement("client", ci.getClient()));

			// Freshdesk Ticket #
			sb.append(Util.printElement("freshdesk", ci.getFreshdesk()));

			// Jira Ticket #
			sb.append(Util.printElement("jira", ci.getJira()));

			// Severity
			sb.append(Util.printElement("severity", ci.getSeverity()));

			// Description & IssueSummary
			sb.append(Util.printElement("summary", ci.getSummary()));

			// Team
			sb.append(Util.printElement("team", ci.getTeam()));

			// Assignee
			sb.append(Util.printElement("assignee", ci.getAssignee()));

			// Release Target (Wed)
			sb.append(Util.printElement("releaseTarget", ci.getReleaseTarget()));

			// Jira Status
			sb.append(Util.printElement("jiraStatus", ci.getStatus()));

			// Resolution
			sb.append(Util.printElement("resolution", ci.getResolution()));

			// Affected Version (issue is identified)
			sb.append(Util.printElement("affectedVersion", ci.getAffected()));

			// Fix/Patch Version
			sb.append(Util.printElement("fixVersion", ci.getPatchVersion()));

			// Comments
			sb.append(Util.printElement("comments", ci.getComments()));

			sb.append("</tr>");
			sb.append("\n");
		} // end for

		sb.append("</table>");

		sb.append("</body></html>");

		return sb.toString();
	}

	//~ ------------------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param   data  DOCUMENT ME!
	 * @return  DOCUMENT ME!
	 */
	public static String printXml(List<CiItem> data)
	{
		StringBuilder sb = new StringBuilder();

		sb.append("<dataset>");
		sb.append("\n");

		for (CiItem ci : data)
		{
			sb.append("<record>");
			sb.append("\n");

			// Client
			sb.append(Util.printElement("Client", ci.getClient()));

			// Freshdesk Ticket #
			sb.append(Util.printElement("Freshdesk", ci.getFreshdesk()));

			// Jira Ticket #
			sb.append(Util.printElement("Jira", ci.getJira()));

			// Severity
			sb.append(Util.printElement("Severity", ci.getSeverity()));

			// Description & IssueSummary
			sb.append(Util.printElement("Summary", ci.getSummary()));

			// Team
			sb.append(Util.printElement("Team", ci.getTeam()));

			// Assignee
			sb.append(Util.printElement("Assignee", ci.getAssignee()));

			// Release Target (Wed)
			sb.append(Util.printElement("ReleaseTarget", ci.getReleaseTarget()));

			// Jira Status
			sb.append(Util.printElement("JiraStatus", ci.getStatus()));

			// Resolution
			sb.append(Util.printElement("Resolution", ci.getResolution()));

			// Affected Version (issue is identified)
			sb.append(Util.printElement("AffectedVersion", ci.getAffected()));

			// Fix/Patch Version
			sb.append(Util.printElement("FixVersion", ci.getPatchVersion()));

			// Comments
			sb.append(Util.printElement("Comments", ci.getComments()));

			sb.append("</record>");
			sb.append("\n");
		} // end for

		sb.append("</dataset>");

		return sb.toString();
	}
}
