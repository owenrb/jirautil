/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package net.friartuck.jira.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class Util
{
	//~ Static fields/initializers ---------------

	/** DOCUMENT ME! */
	public static final String OUTPUT_FORMAT = "XML";

	//~ Methods ----------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param   freshdesk  DOCUMENT ME!
	 * @return  DOCUMENT ME!
	 */
	public static String filterFreshdesk(String freshdesk)
	{
		if (freshdesk == null)
		{
			return "";
		}

		StringBuilder sb = new StringBuilder();
		String sep = "";

		for (String item : freshdesk.split(" "))
		{
			if (item.startsWith("#"))
			{
				sb.append(sep);
				sb.append(item);
				sep = ",";
			}
		}

		return sb.toString();
	}

	//~ ------------------------------------------

	public static String getInfo(String key, String comment)
	{
		Matcher m = Pattern.compile(String.format("\\[%s\\[([^\\]\\]]+)\\]\\]", key)).matcher(
				comment);
		if (m.find())
		{
			return m.group(1);
		}

		return null;
	}

	//~ ------------------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param   element  DOCUMENT ME!
	 * @param   value    DOCUMENT ME!
	 * @return  DOCUMENT ME!
	 */
	public static String printElement(String element, String value)
	{
		if ("HTML".equals(OUTPUT_FORMAT))
		{
			return String.format("<td>%s</td>\n", (value == null) ? " " : value);
		}
		else
		{
			return String.format("<%s>%s</%s>\n", element,
				((value == null) || value.trim().isEmpty()) ? "." : value, element);
		}
	}
}
