/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package net.friartuck.jira.xml.model;

/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class CiItem
{
	//~ Instance fields --------------------------

	private String affected;
	private String assignee;
	private String client;
	private String comments;
	private String freshdesk;
	private String jira;
	private String patchVersion;
	private String releaseTarget;
	private String resolution;
	private String severity;
	private String status;
	private String summary;
	private String team;

	private String workWeek;

	//~ Methods ----------------------------------

	public String getAffected()
	{
		return affected;
	}

	//~ ------------------------------------------

	public String getAssignee()
	{
		return assignee;
	}

	//~ ------------------------------------------

	public String getClient()
	{
		return client;
	}

	//~ ------------------------------------------

	public String getComments()
	{
		return comments;
	}

	//~ ------------------------------------------

	public String getFreshdesk()
	{
		return freshdesk;
	}

	//~ ------------------------------------------

	public String getJira()
	{
		return jira;
	}

	//~ ------------------------------------------

	public String getPatchVersion()
	{
		return patchVersion;
	}

	//~ ------------------------------------------

	public String getReleaseTarget()
	{
		return releaseTarget;
	}

	//~ ------------------------------------------

	public String getResolution()
	{
		return resolution;
	}

	//~ ------------------------------------------

	public String getSeverity()
	{
		return severity;
	}

	//~ ------------------------------------------

	public String getStatus()
	{
		return status;
	}

	//~ ------------------------------------------

	public String getSummary()
	{
		return summary;
	}

	//~ ------------------------------------------

	public String getTeam()
	{
		return team;
	}

	//~ ------------------------------------------

	public String getWorkWeek()
	{
		return workWeek;
	}

	//~ ------------------------------------------

	public void setAffected(String affected)
	{
		this.affected = affected;
	}

	//~ ------------------------------------------

	public void setAssignee(String assignee)
	{
		this.assignee = assignee;
	}

	//~ ------------------------------------------

	public void setClient(String client)
	{
		this.client = client;
	}

	//~ ------------------------------------------

	public void setComments(String comments)
	{
		this.comments = comments;
	}

	//~ ------------------------------------------

	public void setFreshdesk(String freshdesk)
	{
		this.freshdesk = freshdesk;
	}

	//~ ------------------------------------------

	public void setJira(String jira)
	{
		this.jira = jira;
	}

	//~ ------------------------------------------

	public void setPatchVersion(String patchVersion)
	{
		this.patchVersion = patchVersion;
	}

	//~ ------------------------------------------

	public void setReleaseTarget(String releaseTarget)
	{
		this.releaseTarget = releaseTarget;
	}

	//~ ------------------------------------------

	public void setResolution(String resolution)
	{
		this.resolution = resolution;
	}

	//~ ------------------------------------------

	public void setSeverity(String severity)
	{
		this.severity = severity;
	}

	//~ ------------------------------------------

	public void setStatus(String status)
	{
		this.status = status;
	}

	//~ ------------------------------------------

	public void setSummary(String summary)
	{
		this.summary = summary;
	}

	//~ ------------------------------------------

	public void setTeam(String team)
	{
		this.team = team;
	}

	//~ ------------------------------------------

	public void setWorkWeek(String workWeek)
	{
		this.workWeek = workWeek;
	}
}
